package com.example.stan.movies.view.ui;

import android.os.Bundle;
import android.view.Menu;
import android.view.animation.AccelerateDecelerateInterpolator;

import com.example.stan.movies.R;
import com.example.stan.movies.utils.NavigationHost;
import com.example.stan.movies.utils.NavigationIconClickListener;
import com.google.android.material.button.MaterialButton;

import javax.inject.Inject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

public class MainActivity extends AppCompatActivity implements NavigationHost, HasSupportFragmentInjector {

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;

    private final static String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar bottomAppBar = findViewById(R.id.toolbar);
        bottomAppBar.setTitleTextColor(getResources().getColor(R.color.textColorPrimary));

        if (savedInstanceState == null) {
            navigateTo(NowplayingFragment.newInstance(), false);
            bottomAppBar.setTitle(R.string.now_playing);
        }
        MaterialButton nowPlayingButton = findViewById(R.id.now_button);
        MaterialButton upcomingButton = findViewById(R.id.upcoming_button);
        MaterialButton popularButton = findViewById(R.id.popular_button);
        MaterialButton topButton = findViewById(R.id.top_button);

        initViews(bottomAppBar);

        nowPlayingButton.setOnClickListener(view -> {
            navigateTo(NowplayingFragment.newInstance(), false);
            bottomAppBar.setTitle(R.string.now_playing);
        });

        upcomingButton.setOnClickListener(view -> {
            navigateTo(UpcomingFragment.newInstance(), false);
            bottomAppBar.setTitle(R.string.upcoming);
        });

        popularButton.setOnClickListener(view -> {
            navigateTo(PopularFragment.newInstance(), false);
            bottomAppBar.setTitle(R.string.popular);
        });

        topButton.setOnClickListener(view -> {
            navigateTo(TopFragment.newInstance(), false);
            bottomAppBar.setTitle(R.string.top_rated);
        });

    }

    private void initViews(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new NavigationIconClickListener(this,
                findViewById(R.id.main_container),
                new AccelerateDecelerateInterpolator(),
                getResources().getDrawable(R.drawable.ic_nav_open_24dp),
                getResources().getDrawable(R.drawable.ic_nav_close_24dp)));


    }

    @Override
    public void navigateTo(Fragment fragment, boolean addToBackstack) {
        FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_container, fragment);

        if (addToBackstack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_item, menu);
        return true;
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }
}
