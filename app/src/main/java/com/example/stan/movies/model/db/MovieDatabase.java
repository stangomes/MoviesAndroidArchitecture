package com.example.stan.movies.model.db;//package com.stan.moviehubaac.db;

import com.example.stan.movies.model.db.entity.MovieDetailEntity;
import com.example.stan.movies.model.db.entity.NowPlayingEntity;
import com.example.stan.movies.model.db.entity.PopularEntity;
import com.example.stan.movies.model.db.entity.TopEntity;
import com.example.stan.movies.model.db.entity.UpcomingEntity;

import androidx.room.Database;
import androidx.room.RoomDatabase;

/************************
 *Author : Stanley Gomes *
 *Since : 30/05/2018        *
 ************************/

@Database(entities = {MovieDetailEntity.class, NowPlayingEntity.class, PopularEntity.class, TopEntity.class, UpcomingEntity.class}, version = 7, exportSchema = false)
public abstract class MovieDatabase extends RoomDatabase {

    public abstract MovieDao movieDao();


}
