package com.example.stan.movies.di;


import android.app.Application;

import com.example.stan.movies.model.db.MovieDao;
import com.example.stan.movies.model.db.MovieDatabase;
import com.example.stan.movies.model.network.ApiService;

import javax.inject.Singleton;

import androidx.room.Room;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = ViewModelModule.class)
class AppModule {
    @Singleton
    @Provides
    ApiService provideApiService() {
        return new Retrofit.Builder()
                .baseUrl(ApiService.HTTP_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ApiService.class);
    }

    @Singleton
    @Provides
    MovieDatabase provideDatabase(Application application) {
        return Room.databaseBuilder(application, MovieDatabase.class, "movies_db")
                .fallbackToDestructiveMigration()
                .build();
    }


    @Singleton
    @Provides
    MovieDao provideMovieDao(MovieDatabase database) {
        return database.movieDao();
    }
}
