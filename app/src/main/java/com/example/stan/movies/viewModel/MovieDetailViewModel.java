package com.example.stan.movies.viewModel;

import com.example.stan.movies.model.MovieRepository;
import com.example.stan.movies.model.db.entity.MovieDetailEntity;
import com.example.stan.movies.model.network.model.Resource;
import com.example.stan.movies.utils.Constants;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

/************************
 *Author : Stanley Gomes *
 *Since : 03/06/2018        *
 ************************/
public class MovieDetailViewModel extends ViewModel {

    private static final String TAG = MovieDetailViewModel.class.getSimpleName();
    private LiveData<Resource<MovieDetailEntity>> mMovieDetailResponseLiveData;
    private final MovieRepository movieRepository;


    @Inject
    public MovieDetailViewModel(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }


    public LiveData<Resource<MovieDetailEntity>> getMovieDetails(int id) {
        return mMovieDetailResponseLiveData = movieRepository.getMovieDetails(Constants.API_KEY, id);
    }

}
