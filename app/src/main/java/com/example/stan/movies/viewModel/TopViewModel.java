package com.example.stan.movies.viewModel;

import com.example.stan.movies.model.MovieRepository;
import com.example.stan.movies.model.db.entity.TopEntity;
import com.example.stan.movies.model.network.model.Resource;
import com.example.stan.movies.utils.Constants;

import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

/************************
 *Author : Stanley Gomes *
 *Since : 02/06/2018        *
 ************************/

public class TopViewModel extends ViewModel {
    private LiveData<Resource<List<TopEntity>>> mMovieListLiveData;

    @Inject
    public TopViewModel(MovieRepository movieRepository) {
        mMovieListLiveData = movieRepository.getTopList(Constants.API_KEY, "us");
    }

    public LiveData<Resource<List<TopEntity>>> getMovies() {
        return mMovieListLiveData;
    }
}
