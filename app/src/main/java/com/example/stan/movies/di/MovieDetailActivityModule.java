package com.example.stan.movies.di;

import com.example.stan.movies.view.ui.MovieDetailActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MovieDetailActivityModule {
    @ContributesAndroidInjector
    abstract MovieDetailActivity contributeMovieDetailActivity();
}
