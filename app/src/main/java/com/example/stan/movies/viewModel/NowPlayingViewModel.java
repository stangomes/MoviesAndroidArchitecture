package com.example.stan.movies.viewModel;

import com.example.stan.movies.model.MovieRepository;
import com.example.stan.movies.model.db.entity.NowPlayingEntity;
import com.example.stan.movies.model.network.model.Resource;
import com.example.stan.movies.utils.Constants;

import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

/************************
 *Author : Stanley Gomes *
 *Since : 01/06/2018        *
 ************************/
public class NowPlayingViewModel extends ViewModel {

    private final LiveData<Resource<List<NowPlayingEntity>>> mMovieListLiveData;


    @Inject
    public NowPlayingViewModel(MovieRepository movieRepository) {
        mMovieListLiveData = movieRepository.getNowPlayingList(Constants.API_KEY, "us");
    }

    public LiveData<Resource<List<NowPlayingEntity>>> getMovies() {
        return mMovieListLiveData;
    }

}
