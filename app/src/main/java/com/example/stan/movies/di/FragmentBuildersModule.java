package com.example.stan.movies.di;

import com.example.stan.movies.view.ui.NowplayingFragment;
import com.example.stan.movies.view.ui.PopularFragment;
import com.example.stan.movies.view.ui.TopFragment;
import com.example.stan.movies.view.ui.UpcomingFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentBuildersModule {
    @ContributesAndroidInjector
    abstract NowplayingFragment contributeNowPlayingFragment();

    @ContributesAndroidInjector
    abstract UpcomingFragment contributeUpcomingFragment();

    @ContributesAndroidInjector
    abstract PopularFragment contributePopularFragment();

    @ContributesAndroidInjector
    abstract TopFragment contributeTopFragment();
}
