package com.example.stan.movies.di;

import com.example.stan.movies.view.ui.MainActivity;
import com.example.stan.movies.view.ui.MovieDetailActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityModule {
    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract MainActivity contributeMainActivity();

    @ContributesAndroidInjector
    abstract MovieDetailActivity contributeDetailActivity();
}
