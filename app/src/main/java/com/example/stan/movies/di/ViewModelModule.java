package com.example.stan.movies.di;

import com.example.stan.movies.viewModel.MovieDetailViewModel;
import com.example.stan.movies.viewModel.NowPlayingViewModel;
import com.example.stan.movies.viewModel.PopularViewModel;
import com.example.stan.movies.viewModel.TopViewModel;
import com.example.stan.movies.viewModel.UpcomingViewModel;
import com.example.stan.movies.viewModel.ViewModelFactory;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(NowPlayingViewModel.class)
    abstract ViewModel bindNowPlayingViewModel(NowPlayingViewModel nowPlayingViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(PopularViewModel.class)
    abstract ViewModel bindPopularViewModel(PopularViewModel popularViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(UpcomingViewModel.class)
    abstract ViewModel bindUpcomingViewModel(UpcomingViewModel upcomingViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(TopViewModel.class)
    abstract ViewModel bindTopViewModel(TopViewModel topViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(MovieDetailViewModel.class)
    abstract ViewModel bindMovieDetailViewModel(MovieDetailViewModel movieDetailViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);

}
