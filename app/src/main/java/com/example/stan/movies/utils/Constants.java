package com.example.stan.movies.utils;

/************************
 *Author : Stanley Gomes *
 *Since : 02/06/2018        *
 ************************/

public final class Constants {
    public static final String API_KEY = "ed1be966ffc26acd4362636b4e406fe1";
    public static final String POSTER_PATH = "https://image.tmdb.org/t/p/w342/";
    public static final String BACKDROP_PATH = "https://image.tmdb.org/t/p/original/";
}
