package com.example.stan.movies.model.network.model;

public enum Status {
    SUCCESS,
    ERROR,
    LOADING
}
