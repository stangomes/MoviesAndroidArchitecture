package com.example.stan.movies.view.adapter;

public interface MovieClickListener {
    void onItemClickListener(int movieId, String title);
}
