package com.example.stan.movies.viewModel;

import com.example.stan.movies.model.MovieRepository;
import com.example.stan.movies.model.db.entity.UpcomingEntity;
import com.example.stan.movies.model.network.model.Resource;
import com.example.stan.movies.utils.Constants;

import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

/************************
 *Author : Stanley Gomes *
 *Since : 02/06/2018        *
 ************************/

public class UpcomingViewModel extends ViewModel {
    private LiveData<Resource<List<UpcomingEntity>>> mMovieListLiveData;

    @Inject
    public UpcomingViewModel(MovieRepository movieRepository) {
        mMovieListLiveData = movieRepository.getUpcomingList(Constants.API_KEY, "us");
    }

    public LiveData<Resource<List<UpcomingEntity>>> getMovies() {
        return mMovieListLiveData;
    }
}
